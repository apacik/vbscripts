'SolarWinds automatic maintenance using local computer time

Option Explicit

Dim objDir, objFSO, objFile, objConnection, objRecordset, strMember
Dim count, colItems, objWMIService, objItem, CET, CETUntil
Dim UnDate, UnFrom, UnUntil, UnFromTime, UnUntilTime, UnMember, mTime

Const adOpenStatic = 3 'static cursor, forward backward'
Const adLockOptimistic = 3 ' update multiple, which is not locked'

'Get local time
Set objWMIService = GetObject("winmgmts:\\localhost\root\cimv2")
Set colItems = objWMIService.ExecQuery("Select * From Win32_LocalTime")
mTime = Wscript.Arguments.Named.Item("mTime")

For Each objItem in colItems
  CET = objItem.Hour & ":" & objItem.Minute & ":00"
  UnDate = objItem.Month & "/" & objItem.Day & "/" & objItem.Year
Next

If Not IsNumeric(mTime) Then
  wscript.Quit
End if

UnFromTime = FormatDateTime(CET,4)
CETUntil = DateAdd("n", mTime, UnFromTime)
UnUntilTime = FormatDateTime(CETUntil,4)

UnFrom = UnDate & " " & UnFromTime
UnUntil = UnDate & " " & UnUntilTime

Set objFSO = CreateObject("Scripting.FileSystemObject")
objDir = objFSO.GetParentFolderName(wscript.ScriptFullName)
Set objFile = objFSO.OpenTextFile(ObjDir & "\servers.txt")

getconn()

Do until objFile.AtEndOfStream
  strMember = objFile.ReadLine
  If Trim(strMember) <> "" Then
    UnMember = Ucase(strMember)
    Update(UnMember)
  End If
Loop

Function Update(caption)

objRecordSet.Open "SELECT Caption, UnManageFrom, UnManageUntil FROM dbo.Nodes WHERE Caption = '" & _
caption & "'", objConnection, adOpenStatic, adLockOptimistic
count = objRecordSet.RecordCount

select case count
case 0  
    objRecordSet.Close
    wscript.echo(UnMember & ".grouphc.net was not found in Solarwinds.")

case 1
    objRecordSet.Fields.Item("UnManageFrom").Value = UnFrom
    objRecordSet.Fields.Item("UnManageUntil").Value = UnUntil	
    
    if err.number <> 0 then
      wscript.echo err
		  err.Clear                               
	  else
      wscript.echo UnMember & ".grouphc.net was added to Maintenance Mode."
    end if
    objRecordSet.Update
    objRecordSet.Close             

case else
  'move to first record in database'
  objRecordSet.MoveFirst
    do while not objRecordset.EOF
    objRecordSet.MoveNext 'objRecordSet.Update
    loop
    objRecordSet.Close
end select

end Function

Function getconn()
'Create ADO to connect to SQL'
Set objConnection = CreateObject("ADODB.Connection")
Set objRecordSet = CreateObject("ADODB.Recordset")

'Create connection with authentification'
objConnection.Open _
    "Provider=SQLOLEDB;Data Source=DEUSDHEID0053;" & _
        "Trusted_Connection=Yes;Initial Catalog=dbSolarWindsNetPerfMon;Integrated Security=SSPI;"
End Function

wscript.Quit