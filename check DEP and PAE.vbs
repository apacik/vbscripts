Option Explicit

Dim objFSO, objDir, objFileS, objFileL, objWMIService, colItems, objItem, strComputer, strServiceName, sOutput
Dim OSversion, DEPstatus, DEPpolicy, PAEstatus, DEPpolicyNumber

Set objFSO = CreateObject("Scripting.FileSystemObject")
objDir = objFSO.GetParentFolderName(wscript.ScriptFullName)
Set objFileS = objFSO.OpenTextFile(objDir & "\servers.txt")
Set objFileL = objFSO.CreateTextFile(objDir & "\servers.csv", True)

objFileL.writeLine "Server" & VbTab & "OS version" & VbTab & "DEP status" & VbTab & "DEP policy" & VbTab & "PAE status"

Const wbemFlagReturnImmediately = &h10
Const wbemFlagForwardOnly = &h20

On Error Resume Next
Do until objFileS.AtEndOfStream
  strComputer = objFileS.ReadLine
  If Trim(strComputer) <> "" Then
  Wscript.Echo "  Checking... " & strComputer
  Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
    If Err.Number = 0 Then
      Set colItems = objWMIService.ExecQuery("Select * from Win32_OperatingSystem")
      For Each objItem in colItems
        OSversion = objItem.Caption
        DEPstatusValue = objItem.DataExecutionPrevention_Available
        DEPpolicyNumber = objItem.DataExecutionPrevention_SupportPolicy
        PAEstatus = objItem.PAEEnabled
      Next

      WScript.Echo "   Operating system: " & OSversion
      WScript.Echo "   DataExecutionPrevention enabled: " & DEPstatusValue
      WScript.Echo "   DataExecutionPrevention policy: " & DEPpolicyNumber
      WScript.Echo "   PAE enabled: " & PAEstatus

      If DEPstatusValue = True Then 
        DEPstatus = "Enabled"
      Else
        DEPstatus = "Disabled"
      End If
      
      Select Case DEPpolicyNumber
        Case 0
        'DEP is disabled for all processes.
        DEPpolicy = "Disabled for all processes"
        Case 1
        'DEP is enabled for all processes.
        DEPpolicy = "Enabled for all processes"
        Case 2
        'DEP is enabled for only Windows system components and services. (Default)
        DEPpolicy = "Enabled only for Windows system components and services"
        Case 3
        'DEP is enabled for all processes
        DEPpolicy = "Enabled for all processes"
      End Select

      If objItem.PAEEnabled = True Then
        PAEstatus = "Enabled"
      Else
        PAEstatus = "Disabled"
      End If
      
      'Wscript.Echo "  Server access ok!"
      sOutput = strComputer & VbTab & OSversion & VbTab & DEPstatus & VbTab & DEPpolicy & VbTab & PAEstatus
      objFileL.WriteLine(sOutput)
      Wscript.Echo "  Server values saved to log."
    Else
      Wscript.Echo "  Server access error!"
      objFileL.writeLine strComputer & VbTab & "server access error!"
    End If
  End If
  Err.Clear
  wscript.echo
Loop

objFileS.Close
objFileL.Close

Wscript.echo "*********************"
Wscript.echo "  Done, check log !"
wscript.echo
wscript.echo "Press the ENTER key to exit."

Do While Not WScript.StdIn.AtEndOfLine
   Input = WScript.StdIn.Read(1)
Loop
wscript.quit