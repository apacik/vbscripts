'Get list of members from group grouphc.net/GIT/Groups/GRP SMS * Patch management testing computers, * specified in CountryList

Option Explicit
Dim objFSO, objDir, objFile, CountryList, strHolder, i, j, countryCode, GroupName, objGroup, arrMemberOf, strMember

'Prepare output file
Set objFSO = CreateObject("Scripting.FileSystemObject")
objDir = objFSO.GetParentFolderName(wscript.ScriptFullName)
Set objFile = objFSO.CreateTextFile(objDir & "\Patch management testing computers.csv", True)
objFile.writeLine "Computer Name (membership)" & vbTab & "Country Code" & vbTAB & "AD Group name"

'Predefined list of countries
CountryList = Array("AFR","AUS","BIH","BNL","CZE","DEN","DEU","ESP","EST","GBR","GEO","HRV","HUN","IND","ISR","KAZ","LVA","NAM","NOR","POL","ROU","RUS","SGP","SWE","TUR","UKR")

'Sort list of countries
For i = (UBound(CountryList) - 1) to 0 Step -1
  For j = 0 to i
    If UCase(CountryList(j)) > UCase(CountryList(j+1)) Then
      strHolder = CountryList(j+1)
      CountryList(j+1) = CountryList(j)
      CountryList(j) = strHolder
    End If
  Next
Next

Set i = Nothing
Set j = Nothing

'Read groups one by one
For Each countryCode in CountryList

  'Skip error if there are no members in group and continue
  On Error Resume Next

  'Get members of specific group
  GroupName = "GRP SMS " & countryCode & " Patch management testing computers"
  Set objGroup = GetObject("LDAP://cn=" & GroupName & ",OU=Groups,OU=GIT,DC=grouphc,DC=net")
  arrMemberOf = objGroup.GetEx("member")

  'Report any error
  On Error GoTo 0

  'Sort list of members
  For i = (UBound(arrMemberOf) - 1) to 0 Step -1
    For j = 0 to i
      If UCase(arrMemberOf(j)) > UCase(arrMemberOf(j+1)) Then
        strHolder = arrMemberOf(j+1)
        arrMemberOf(j+1) = arrMemberOf(j)
        arrMemberOf(j) = strHolder
      End If
    Next
  Next

  Set i = Nothing
  Set j = Nothing

  'Write list with country code and group name to output file
  For Each strMember In arrMemberOf
    objFile.writeLine (UCase(Split(Mid(strMember,4), ",")(0))) & vbTab & countryCode & vbTab & GroupName
  Next

Next

'End
objFile.Close
Wscript.Echo "List was created, check .csv file."
Wscript.Quit