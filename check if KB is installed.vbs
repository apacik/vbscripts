Option Explicit

Dim objFSO, objDir, objFileS, objFileL, objWMIService, colItems, objItem, strComputer, strServiceName, sOutput
Dim strHotfixID, HotFixStatus, tal

Set objFSO = CreateObject("Scripting.FileSystemObject")
objDir = objFSO.GetParentFolderName(wscript.ScriptFullName)
Set objFileS = objFSO.OpenTextFile(objDir & "\servers.txt")
Set objFileL = objFSO.CreateTextFile(objDir & "\servers.csv", True)

strHotfixID = InputBox("Enter KB number of hotfix to check")
objFileL.writeLine "Server,KB" & strHotfixID

If LEN(strHotfixID) = 0 Then
  wscript.quit
End If

Do until objFileS.AtEndOfStream
  strComputer = objFileS.ReadLine
  If Trim(strComputer) <> "" Then
  
    On error resume next
    
    wscript.echo
    wscript.echo("Connecting to... " & strComputer)
    Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
    If Err.Number = 0 Then
      wscript.echo("Getting WMI information...")
      Set colItems = objWMIService.ExecQuery("Select * from Win32_QuickFixEngineering where HotFixID = 'Q" &_
                     strHotfixID & "' OR HotFixID = 'KB" & strHotfixID & "'")
      If Err.Number = 0 Then
      tal = colItems.count
        If tal > 0 Then
          HotfixStatus = "Installed"
          wscript.echo("KB" & strHotfixID & " is installed.")
        Else
          HotfixStatus = "Not installed"
          wscript.echo("KB" & strHotfixID & " is not installed.")
        End If
      Else
        HotfixStatus = "Unable to get WMI hotfix info"
        wscript.echo("Unable to get WMI hotfix info")
      End if
    Else
      HotfixStatus = "WMI could not connect to computer"
      wscript.echo("WMI could not connect to computer")
      objFileL.writeLine strComputer & VbTab & HotfixStatus  
    End If
    
    Err.Clear
    objFileL.writeLine strComputer & "," & HotfixStatus
    set objWMIService = Nothing
    set colItems = Nothing
    
  End If
Loop

objFileS.Close
objFileL.Close

wscript.echo
wscript.echo "Done, check log !"
wscript.sleep 3000

wscript.quit