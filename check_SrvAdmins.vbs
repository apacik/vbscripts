'Description: check local administrators group in list of server if exist *SrvAdmin account and print full name, expiration and lock flags

Option Explicit

Dim objFSO, objDir, objFileR, objFileW, strComputer, objGroup, objMember, objUser, User, Search, flag, Locked, Unexpired

Set objFSO = CreateObject("Scripting.FileSystemObject")
objDir = objFSO.GetParentFolderName(wscript.ScriptFullName)
Set objFileR = objFSO.OpenTextFile(objDir & "\servers.txt")

If objFSO.FileExists(objDir & "\SrvAdmins.csv") Then
  Set objFileW = objFSO.OpenTextFile(objDir & "\SrvAdmins.csv",8,True)
Else
  Set objFileW = objFSO.CreateTextFile(objDir & "\SrvAdmins.csv",True)
  objFileW.WriteLine "Computer;SrvAdmin acc.name;Pas.Unexpired?;Acc.Locked?"
End If

On Error Resume Next

Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000
Const ADS_UF_ACCOUNTDISABLE = &H02

Wscript.Echo

Do until objFileR.AtEndOfStream
  strComputer = objFileR.ReadLine
  If Trim(strComputer) <> "" Then
    Wscript.Echo "  Checking..." & strComputer
    Set objGroup = GetObject("WinNT://" & strComputer & "/Administrators,group")
    If Err.Number = 0 Then
      Search = False
      For Each objUser In objGroup.Members
        If right(LCase(objUser.Name),8) = "srvadmin" Then
          Search = True
          User = objUser.Name
          flag = objUser.Get("UserFlags")
          If flag AND ADS_UF_ACCOUNTDISABLE Then
            Locked = "Yes"
          Else
            Locked = "No"
          End If
          If flag AND ADS_UF_DONT_EXPIRE_PASSWD Then
            Unexpired = "Yes"
          Else
            Unexpired = "No"
          End If
        End If
      Next
      If Search = True Then
          objFileW.WriteLine strComputer & ";" & User & ";" & Unexpired & ";" & Locked
      Else
          objFileW.WriteLine strComputer & ";Missing"
      End If
    Else
      objFileW.WriteLine strComputer & ";Error"
    End If
  End If
  Err.Clear
Loop

objFileR.Close
objFileW.Close

Set objGroup = Nothing
Set objUser = Nothing

Wscript.Echo
Wscript.Echo "  Done, check results in SrvAdmins.csv"
Wscript.Echo

Wscript.Quit