Option explicit

'Works only for GROUPHC domain!

Dim objFSO, objFile, strGroupName, oGroup, objGroup, i, j, strHolder, arrMemberOf, strMember, strComputer, sText, objTrans, objSystemInfo, strDomain, strComputerDN

' Constants required for name translate
Const ADS_NAME_INITTYPE_GC = 3
Const ADS_NAME_TYPE_NT4 = 3
Const ADS_NAME_TYPE_1779 = 1
Const ADS_PROPERTY_DELETE = 4

'Get the NETBIOS name of the domain
Set objSystemInfo = CreateObject("ADSystemInfo") 
strDomain = objSystemInfo.DomainShortName

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.OpenTextFile("servers.txt", 1)

strGroupName = "CN=GRP ADS pw1 g1,OU=Groups,OU=GIT,DC=grouphc,DC=net"

Set oGroup = GetObject("LDAP://" & strGroupName)

Do until objFile.AtEndOfStream

strMember = objFile.ReadLine
   If Trim(strMember) <> "" Then
   strComputerDN = getComputerDN(strMember,strDomain)
   WScript.Echo strComputerDN
   oGroup.PutEx ADS_PROPERTY_DELETE, "member", Array(strComputerDN)
   oGroup.SetInfo
   End If
Loop

set oGroup = Nothing

objFile.Close

set oGroup = Nothing

Wscript.echo "Done!"
Wscript.Quit


' Call function to return the distinguished name (DN) of the computer
function getComputerDN(byval strComputer,byval strDomain)
	' Function to get the distinguished name of a computer
	' from the NETBIOS name of the computer (strcomputer)
	' and the NETBIOS name of the domain (strDomain) using
	' name translate

	Set objTrans = CreateObject("NameTranslate")
	' Initialize name translate using global catalog
	objTrans.Init ADS_NAME_INITTYPE_GC, ""
	' Input computer name (NT Format)
	objTrans.Set ADS_NAME_TYPE_NT4, strDomain & "\" & strComputer & "$"
	' Get Distinguished Name.
	getComputerDN = objTrans.Get(ADS_NAME_TYPE_1779)

end function