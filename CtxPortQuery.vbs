'Ctx port query and reset
Option Explicit
Dim objShell, objFSO, objDir, objExec, objPing, strPingResults, strCommand, warningLevel, intAnswer
Dim strServer, portList, portNumber, portStatus, portName, portStatusAgain

'Initializating
Set objShell = WScript.CreateObject("WScript.Shell")
Set objFSO = CreateObject("Scripting.FileSystemObject")
objDir = objFSO.GetParentFolderName(wscript.ScriptFullName)
strCommand = objFSO.GetFile(objDir & "\portqry.exe")

Wscript.Echo
Wscript.Echo "**********************************************"
Wscript.Echo " Port query and reset tool for Citrix servers "
Wscript.Echo "**********************************************"

'Main program (asked if run again after finish)
Do
  'Get server name for check, exit if no input or Cancel
  strServer = InputBox("Enter server name to check:","Port query and reset tool")
  If LEN(strServer) = 0 Then
    Wscript.Quit
  End If

  'Checking ping to server, exit if no ping
  Wscript.Echo
  Wscript.Echo "Checking " & strServer & ".."
  strPingResults = objShell.Run("ping -n 3 -w 3000 " & strServer,0,True)
  If strPingResults = 0 Then
    WScript.Echo "Ping status: OK"
    objPing = True
  Else
    WScript.Echo "Ping status: Failed!"
    Wscript.Echo
    Wscript.Echo "Cannot continue, please check server availability!"
    objPing = False
  End If

  'List of ports to check
  portList = Array("3389","1494","2512")

  'Query status of defined ports and reset RDP or ICA if not listening
  If objPing = True Then
    For Each portNumber in portList

      portStatus = portQuery(portNumber)
      If portNumber = "3389" Then portName = "RDP" End If
      If portNumber = "1494" Then portName = "ICA" End If
      If portNumber = "2512" Then portName = "IMA" End If

      If portStatus = 0 or portStatus = 2 Then
        Wscript.Echo (portName & " status: OK")
      End If

      If portStatus = 1 Then
          Wscript.Echo
          Wscript.Echo (portName & " status: Failed!")
          Wscript.Echo " Reseting " & portName & " session.."
          'Wscript.Sleep 1000
          Set objExec = objShell.Exec("reset session " & portName & "-tcp /server:" & strServer)
          Wscript.Sleep 1000
          Wscript.Echo (" " & portName & " connection reseted, checking again..")
          Wscript.Sleep 3000
          portStatusAgain = portQuery(portNumber)
          If portStatusAgain = 0 or portStatusAgain = 2 Then
            Wscript.Echo (portName & " status: OK")
          Else
            Wscript.Echo (portName & " status: Failed again!")
            warningLevel = 1
          End If
      End If
    Next
  End If
  
  'Show warning if port reset didn't help
  If warningLevel = 1 Then
    Wscript.Echo
    Wscript.Echo "Some ports have status 'Failed' after reset action."
    Wscript.Echo "Please create SDE ticket!"
  Else
    If objPing = True Then
      Wscript.Echo
      Wscript.Echo "All ports are available."
    End If
  End If
 
  'Ask if run again, else exit
  intAnswer = Msgbox("Do you want to check another server?", vbYesNo, "Port query and reset tool")
  If intAnswer = vbNo Then
    Wscript.Quit
  End If
  warningLevel = 0
  Set objPing = Nothing
  Wscript.Echo
  Wscript.Echo "---------------------------------------------------"
Loop

'Function for portqry
Function portQuery(portNumber)
  Set objExec = objShell.Exec(strCommand & " -n " & strServer & " -e " & portNumber & " -q -s")
  While objExec.Status = 0
    WScript.Sleep 100
  Wend
  portQuery = objExec.ExitCode
End Function