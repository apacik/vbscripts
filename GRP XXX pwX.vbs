'*****************************************************'
' File:    GRP XXX pwX.vbs                            '
' Author:  Ales Pacik, SDO Mokra 2011                 '
'                                                     '
' Retrieving members of AD groups into .txt files for '
' patching in Auto (GRP XXX pwX A.txt)                '
' and Manual (GRP XXX pwX M.txt) restart              '
'*****************************************************'

Option Explicit

Dim objFSO, pwNumber, countryCode, objFileM, objFileA, objGroupM, arrMemberOfM, objGroupA, arrMemberOfA, duplicate, i, j, strHolder, strMemberM, strMemberA
Dim objGroupAerror, objGroupMerror, GroupEcho

Set objFSO = CreateObject("Scripting.FileSystemObject")

'Get wave and country from script file name
pwNumber = Split((Mid(Wscript.ScriptName,11)),".vbs")(0)
countryCode = Split((Mid(Wscript.ScriptName,5))," pw")(0)

'Skip error if there are no members in groups
On Error Resume Next
objGroupAerror = 0
objGroupMerror = 0

'Read group A
Set objGroupA = GetObject("LDAP://CN=GRP " & countryCode & " pw" & pwNumber & " A,OU=Groups,OU=GIT,DC=grouphc,DC=net")
arrMemberOfA = objGroupA.GetEx("member")

'If error occurs, show warning
If Err.Number <> 0 Then
  If Err.Number = 424 Then
    Wscript.Echo "GRP " & countryCode & " pw" & pwNumber & " A group does not exist !"
    objGroupAerror = 1
  End If
  If Err.Number = -2147463155 Then
    GroupEcho = "GRP " & countryCode & " pw" & pwNumber & " A group is empty.."
    objGroupAerror = 1
  End If
  On Error GoTo 0
End If

On Error Resume Next
'Read group M
Set objGroupM = GetObject("LDAP://CN=GRP " & countryCode & " pw" & pwNumber & " M,OU=Groups,OU=GIT,DC=grouphc,DC=net")
arrMemberOfM = objGroupM.GetEx("member")

'If error occurs, show warning
If Err.Number <> 0 Then
  If Err.Number = 424 Then
    Wscript.Echo "GRP " & countryCode & " pw" & pwNumber & " M group does not exist !"
    objGroupMerror = 1
  End If
  If Err.Number = -2147463155 Then
    GroupEcho = "GRP " & countryCode & " pw" & pwNumber & " M group is empty.."
    objGroupMerror = 1
  End If
End If

On Error GoTo 0

'Create file for Auto restart members
Set objFileA = objFSO.CreateTextFile("GRP " & countryCode & " pw" & pwNumber & " A.txt",true)

'If arrMemberOfA is not empty
If objGroupAerror = 0 Then
    'Sort Auto restart members alphabetically
    For i = (UBound(arrMemberOfA) - 1) to 0 Step -1
      For j = 0 to i
        If UCase(arrMemberOfA(j)) > UCase(arrMemberOfA(j+1)) Then
          strHolder = arrMemberOfA(j+1)
          arrMemberOfA(j+1) = arrMemberOfA(j)
          arrMemberOfA(j) = strHolder
        End If
      Next
    Next

  Set i = Nothing
  Set j = Nothing

  'Write Auto restart members
  For Each strMemberA In arrMemberOfA
    objFileA.WriteLine(UCase(Split(Mid(strMemberA,4), ",")(0)))
  Next
End If

Set objGroupA = Nothing
Set strHolder = Nothing

objFileA.Close

'If A group is empty, delete file
Set objFileA = objFSO.GetFile("GRP " & countryCode & " pw" & pwNumber & " A.txt")
If objFileA.Size = 0 Then
 objFileA.Delete
End If

'Create file for manual restart members
Set objFileM = objFSO.CreateTextFile("GRP " & countryCode & " pw" & pwNumber & " M.txt",true)

If objGroupMerror = 0 Then 
  'Sort manual restart members alphabetically
  For i = (UBound(arrMemberOfM) - 1) to 0 Step -1
    For j = 0 to i
      If UCase(arrMemberOfM(j)) > UCase(arrMemberOfM(j+1)) Then
        strHolder = arrMemberOfM(j+1)
        arrMemberOfM(j+1) = arrMemberOfM(j)
        arrMemberOfM(j) = strHolder
      End If
    Next
  Next

  Set i = Nothing
  Set j = Nothing

  'Write Manual restart members
  For Each strMemberM In arrMemberOfM
    objFileM.WriteLine(UCase(Split(Mid(strMemberM,4), ",")(0)))      
  Next
End If

Set objGroupM = Nothing
Set strHolder = Nothing

objFileM.Close

'If M group is empty, delete file
Set objFileM = objFSO.GetFile("GRP " & countryCode & " pw" & pwNumber & " M.txt")
If objFileM.Size = 0 Then
 objFileM.Delete
End If

If objGroupAerror = 1 And objGroupMerror = 1 Then
  Wscript.Quit
Else
  Wscript.Echo "GRP " & countryCode & " pw" & pwNumber & " done." & vbCr & GroupEcho
  Wscript.Quit
End If